package cn.jasonone.at;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

/**
 * AutoTable 入口类
 * 
 * @author Jason
 *
 */
@Configuration
@ComponentScan("cn.jasonone.at")
@MapperScan("cn.jasonone.at.mapper")
public class AutoTableConfiguration {

	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		YamlPropertiesFactoryBean factoryBean = new YamlPropertiesFactoryBean();
		factoryBean.setResources(new ClassPathResource("auto-table.yml"));
		configurer.setProperties(factoryBean.getObject());
		return configurer;
	}

}
