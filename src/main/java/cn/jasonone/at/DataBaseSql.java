package cn.jasonone.at;

import cn.jasonone.at.type.AutoTableType;
import lombok.Data;

/**
 * 数据库SQL配置
 * 
 * @author Jason
 *
 */
@Data
public class DataBaseSql {
	/**
	 * 创建表SQL
	 */
	private String createTableSql;
	/**
	 * 删除表SQL
	 */
	private String dropTableSql;
	/**
	 * 创建列SQL
	 */
	private String createColumnSql;
	/**
	 * 删除列SQL
	 */
	private String dropColumnSql;
	/**
	 * 修改列SQL
	 */
	private String updateColumnSql;
	/**
	 * 创建主键
	 */
	private String createPrimaryKey;

	/**
	 * 创建主键<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#OLD_PK_SIZE_VARIABLE oldPkSize}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getCreatePrimaryKey() {
		return createPrimaryKey;
	}

	/**
	 * 创建表SQL<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#COMMENT_VARIABLE comment}</li>
	 * <li>{@link AutoTableType#ENGINE_VARIABLE engine}</li>
	 * <li>{@link AutoTableType#TEMP_COLUMN_NAME_VARIABLE tempColumnName}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getCreateTableSql() {
		return createTableSql;
	}

	/**
	 * 删除表SQL<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#COMMENT_VARIABLE comment}</li>
	 * <li>{@link AutoTableType#ENGINE_VARIABLE engine}</li>
	 * <li>{@link AutoTableType#TEMP_COLUMN_NAME_VARIABLE tempColumnName}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getDropTableSql() {
		return dropTableSql;
	}

	/**
	 * 创建列SQL<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#COMMENT_VARIABLE comment}</li>
	 * <li>{@link AutoTableType#NOT_NULL_VARIABLE engine}</li>
	 * <li>{@link AutoTableType#TYPE_VARIABLE type}</li>
	 * <li>{@link AutoTableType#COLUMN_NAME_VARIABLE columnName}</li>
	 * <li>{@link AutoTableType#AUTOINCREMENT_VARIABLE autoincrement}</li>
	 * <li>{@link AutoTableType#DEFAULT_VALUE_VARIABLE defaultValue}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getCreateColumnSql() {
		return createColumnSql;
	}

	/**
	 * 删除列SQL<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#COMMENT_VARIABLE comment}</li>
	 * <li>{@link AutoTableType#NOT_NULL_VARIABLE engine}</li>
	 * <li>{@link AutoTableType#TYPE_VARIABLE type}</li>
	 * <li>{@link AutoTableType#COLUMN_NAME_VARIABLE columnName}</li>
	 * <li>{@link AutoTableType#AUTOINCREMENT_VARIABLE autoincrement}</li>
	 * <li>{@link AutoTableType#DEFAULT_VALUE_VARIABLE defaultValue}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getDropColumnSql() {
		return dropColumnSql;
	}

	/**
	 * 修改列SQL<br>
	 * 有效变量:<br>
	 * <ul>
	 * <li>{@link AutoTableType#CATALOG_VARIABLE catalog}</li>
	 * <li>{@link AutoTableType#TABLE_NAME_VARIABLE tableName}</li>
	 * <li>{@link AutoTableType#COMMENT_VARIABLE comment}</li>
	 * <li>{@link AutoTableType#NOT_NULL_VARIABLE engine}</li>
	 * <li>{@link AutoTableType#TYPE_VARIABLE type}</li>
	 * <li>{@link AutoTableType#COLUMN_NAME_VARIABLE columnName}</li>
	 * <li>{@link AutoTableType#AUTOINCREMENT_VARIABLE autoincrement}</li>
	 * <li>{@link AutoTableType#DEFAULT_VALUE_VARIABLE defaultValue}</li>
	 * </ul>
	 * 
	 * @return sql模版代码
	 */
	public String getUpdateColumnSql() {
		return updateColumnSql;
	}

}
