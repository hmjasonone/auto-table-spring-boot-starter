package cn.jasonone.at.type;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Component;

import cn.jasonone.at.DataBaseSql;
import cn.jasonone.at.model.ColumnInfo;
import cn.jasonone.at.model.TableInfo;
import cn.jasonone.at.util.DataBaseUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * create模式
 * 
 * @author Jason
 *
 */
@Component
@Slf4j
public class CreateAutoTableType implements AutoTableType {

	@Override
	public void autoTable(DataBaseSql dbSql, List<String> sqlList, List<TableInfo> tables) throws SQLException {
		for (TableInfo tableInfo : tables) {
			if (tableInfo.getColumns() == null || tableInfo.getColumns().isEmpty()) {
				log.warn("表[{}]中至少需要一个字段", tableInfo.getTableName());
				continue;
			}
			// 检测表是否存在,如果存在则删除表
			if (DataBaseUtil.isTable(tableInfo)) {
				sqlList.add(parseSqlByTable(dbSql.getDropTableSql(), tableInfo));
			}
			// 创建表
			sqlList.add(parseSqlByTable(dbSql.getCreateTableSql(), tableInfo));
			// 原表主键数量
			int oldPkSize = 0;
			// 创建列
			for (ColumnInfo columnInfo : tableInfo.getColumns()) {
				if (Boolean.TRUE.equals(columnInfo.getAutoincrement())) {
					oldPkSize++;
				}
				sqlList.add(parseSqlByColumn(dbSql.getCreateColumnSql(), columnInfo));
			}
			// 删除临时字段
			sqlList.add(parseSqlByColumn(dbSql.getDropColumnSql(), tableInfo.getTempColumn()));
			// 检测表是否含有主键
			if (tableInfo.getPrimaryKeys() != null && !tableInfo.getPrimaryKeys().isEmpty()) {
				sqlList.add(parseSqlByPrimaryKey(dbSql.getCreatePrimaryKey(), tableInfo, oldPkSize));
			}
		}
	}

}
