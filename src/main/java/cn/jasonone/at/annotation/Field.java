package cn.jasonone.at.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * 字段映射注解
 * 
 * @author Jason
 *
 */
@Inherited
@Retention(RUNTIME)
@Target(FIELD)
public @interface Field {
	@AliasFor("name")
	String value() default "";

	@AliasFor("value")
	String name() default "";

	/**
	 * 值转换器
	 * 
	 * @return 转换器类型
	 */
	Class[] converter() default {};
}
