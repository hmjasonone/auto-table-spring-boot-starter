package cn.jasonone.at.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import cn.jasonone.at.AutoTableConfiguration;

/**
 * 开启AutoTable
 * 
 * @author Jason
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
@Inherited
@Import(AutoTableConfiguration.class)
public @interface EnableAutoTable {
}
