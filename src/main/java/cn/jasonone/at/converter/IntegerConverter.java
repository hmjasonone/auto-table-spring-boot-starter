package cn.jasonone.at.converter;

import java.sql.DatabaseMetaData;

import org.springframework.stereotype.Component;

import cn.jasonone.at.model.ColumnInfo;

/**
 * Integer转Boolean
 * 
 * @author Jason
 *
 */
@Component
public class IntegerConverter implements Converter<Integer, Boolean> {

	private static final String NULLABLE = "NULLABLE";

	@Override
	public boolean isConverter(Class<?> clazz) {
		if (clazz == ColumnInfo.class) {
			return true;
		}
		return false;
	}

	@Override
	public Boolean converter(Class<?> clazz, String columnName, Integer sourceValue) {
		if (clazz == ColumnInfo.class) {
			if (NULLABLE.equals(columnName)) {
				if (DatabaseMetaData.columnNoNulls == sourceValue) {
					return false;
				} else if (DatabaseMetaData.columnNullable == (sourceValue)) {
					return true;
				}
			}
		}
		return null;
	}

}
