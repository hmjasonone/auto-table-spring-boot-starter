package cn.jasonone.at;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import cn.jasonone.at.enums.ATType;
import cn.jasonone.at.enums.JdbcType;
import cn.jasonone.at.type.AutoTableType;
import lombok.Data;

/**
 * 核心配置类
 * 
 * @author Jason
 *
 */
@Data
@Configuration
@PropertySource("classpath:auto-table.yml")
@ConfigurationProperties("auto-table")
public class AutoTableProperties {
	/**
	 * 生成模式
	 */
	private ATType type;
	/**
	 * 模式处理器
	 */
	private Map<ATType, Class<? extends AutoTableType>> handlers;
	/**
	 * 数据库SQL配置
	 */
	private Map<String, DataBaseSql> sql;
	/**
	 * 类型转换映射
	 */
	private Map<String, Map<JdbcType, String>> jdbcTypes;
	/**
	 * Java类到jdbcType映射
	 */
	private Map<Class<?>, JdbcType> javaTypes;

	/**
	 * 临时字段名称
	 */
	private String tempColumnName;
	/**
	 * 类别名称: 在Mysql中对应数据库名称,在Oracle中对应实例名称
	 */
	private String catalog;
	/**
	 * bean所在包配置
	 */
	private List<String> baseBeanPackages;
}
