package cn.jasonone.at.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import lombok.Getter;
/**
 * Spring容器上下文
 * @author Jason
 *
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {
	@Getter
	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextUtil.context = applicationContext;
	}
	
	public static <T> List<T> getBeans(Class<T> type){
		Map<String, T> types = context.getBeansOfType(type);
		return new ArrayList<T>(types.values());
	}

}
