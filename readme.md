# 写在前面
由于本工具是基于`Springboot+Mybatis`环境运行,所以使用本工具的童鞋,我一律认为你们已经熟练掌握`Springboot+Mybatis`的使用,并且已经搭建好环境
# 运行环境
+ JDK: 1.8+
+ Springboot: 2.x
+ Mybatis: 3.5.x

# 快速开始
## 1.引入Jar
```
<dependency>
	<groupId>cn.jasonone.autotable</groupId>
	<artifactId>auto-table-spring-boot-starter</artifactId>
	<version>1.0.3</version>
</dependency>
```
## 2.创建实体
```
@Data
@Table(name = "user_info", comment = "用户信息表")
public class UserInfo {
	@Column(autoincrement = true,primaryKey = true,comment = "主键")
	private Integer id;
	@Column(length = 50,primaryKey = true,notNull = true,comment = "用户名")
	private String username;
	@Column(length = 32,notNull = true,comment = "密码")
	private String password;
	@Column(length = 1,defaultValue = "1")
	private Integer status;
	@Column(name="create_time")
	private Date createTime;
	@Column(name="update_time",type = JdbcType.TIMESTAMP)
	private Date updateTime;
}
```
## 3.`application.yml`配置
```
auto-table:
  catalog: test #表类型
```
>注: 表类型比较抽象,因为它在不同数据库中有不同的理解,比如在Mysql中代表的是数据库名称,而Oracle中代表的则是数据库实例名
## 4.启用AutoTable
```
@EnableAutoTable //启用AutoTable
@SpringBootApplication
public class AutoTableExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoTableExampleApplication.class, args);
	}

}
```
# 配置
## 默认配置
```
auto-table:
  type: none #运行模式
  temp-column-name: $_auto_table_temp #临时字段,用来解决无法创建空表的问题
  handlers:
    create: cn.jasonone.at.type.CreateAutoTableType #create模式处理器
    update: cn.jasonone.at.type.UpdateAutoTableType #update模式处理器
  jdbc-types: #对应的数据类型模版字符串
    mysql: #数据库类型,如: mysql,oracle 等等
      varchar: varchar(@{length>0?length:255})
      float: float(@{length>0?length:11},@{decimal>0?decimal:2})
      double: double(@{length>0?length:11},@{decimal>0?decimal:2})
      integer: int(@{length>0?length:11})
      bigint: bigint(@{length>0?length:11})
      bit: bit(1)
      timestamp: timestamp
      date: datetime
  java-types: #在@Column注解中type属性留空时,会自动根据当前属性的java类型,进行映射,以下是映射规则
    java.lang.String: varchar  
    java.util.Date: date
    java.lang.Integer: integer
    java.lang.Double: double
    java.lang.Float: float
    java.lang.Long: bigint
    java.lang.Short: integer    
    java.lang.Boolean: bit
    java.sql.Timestamp: timestamp
  sql:  #不同数据库的DDL模版
    mysql: 
      create-primary-key: alter table `@{catalog}`.`@{tableName}` drop primary key, add primary key (@{for(pk:primaryKeys ,) '`'+pk+'`'}) USING BTREE
      create-column-sql: alter table `@{catalog}`.`@{tableName}` add `@{columnName}` @{type} @{notNull?'not null':''} @{autoincrement?'primary key AUTO_INCREMENT':''} @{defaultValue?("default '"+defaultValue+"'"):''} @{comment?(" comment '"+comment+"'"):''} 
      create-table-sql: create table `@{catalog}`.`@{tableName}`(`@{tempColumnName}` int) comment '@{comment}' 
      drop-column-sql: alter table `@{catalog}`.`@{tableName}` drop `@{columnName}`
      drop-table-sql: drop table `@{catalog}`.`@{tableName}`
      update-column-sql: alter table `@{catalog}`.`@{tableName}` modify `@{columnName}` @{type} @{notNull?'not null':''} @{comment?(" comment '"+comment+"'"):''}
        
          
```
## 必须配置
```
auto-table:
  catalog: test #表类型
```
>注: 表类型比较抽象,因为它在不同数据库中有不同的理解,比如在Mysql中代表的是数据库名称,而Oracle中代表的则是数据库实例名
# DDL模版语法

## 基础语法
### 选择结构
```
	#{表达式 ? 分支1 : 分支2} 
```
### 默认值操作符
>如果值无意义,则使用默认值
```
	#{值!默认值} 
```
### 循环操作
```
	#{for(迭代变量:迭代集合 分隔符) 迭代内容}
```
# 二次开发
## 模式扩展类
### 1.模式扩展类必须实现`cn.jasonone.at.type.AutoTableType`接口,以下为默认实现的模式处理器:
+ cn.jasonone.at.type.CreateAutoTableType
+ cn.jasonone.at.type.UpdateAutoTableType
### 2.必须在配置文件中将模式与模式处理器映射起来
```
  handlers:
    create: cn.jasonone.at.type.CreateAutoTableType #create模式处理器
    update: cn.jasonone.at.type.UpdateAutoTableType #update模式处理器
```
### 3.扩展模式枚举对象`cn.jasonone.at.enums.ATType`对象.
```
/**
 * 生成模式
 * @author Jason
 *
 */
public enum ATType {
	/**
	 * 创建模式: 将删除所有已存在的表,重新创建,会清除所有数据
	 */
	CREATE,
	/**
	 * 更新模式: 保留已存在的表以及字段,不针对字段或其结构进行修改,不影响数据
	 */
	UPDATE,
	/**
	 * 默认模式: 不做任何操作
	 */
	NONE;
}
```
>注:该对象之所以写成枚举,是为了大家在配置时能够具有友好的提示功能

# 历史版本
## 1.1.0
+ 1.新增实体包指定参数`auto-table.base-bean-packages`,提升扫描速度,如未配置将自动扫描当前项目中的实体,而不再扫描整个classpath中的实体
## 1.0.3
+ 1.修复使用Base实体时无法获取到父类注解字段的问题
+ 2.修复空表添加主键时发生错误的问题
+ 3.移除Mybatis,使用原生JDBC进行表的创建
## 1.0.2
+ 1.已支持Mysql(理论上所有支持jdbc的所有数据库都适用)
+ 2.支持主键,自增键,非空,默认值,注释等常见配置


